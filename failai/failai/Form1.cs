﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace failai
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void open_Click(object sender, EventArgs e)
        {
            textBox1.Text = File.ReadAllText(@"d:\Desktop\failai\file.txt");
        }

        private void close_Click(object sender, EventArgs e)
        {
            File.WriteAllText(@"d:\Desktop\failai\file.txt", textBox1.Text);
        }
    }
}
